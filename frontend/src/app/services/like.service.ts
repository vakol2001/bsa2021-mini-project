import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService) { }

    public likePost(post: Post, currentUser: User, isLike: boolean) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: isLike,
            userId: currentUser.id
        };

        // update current array instantly
        let likes = innerPost.reactions.filter((x) => x.user.id === currentUser.id);
        if (likes.length === 0) {
            innerPost.reactions = innerPost.reactions.concat({ isLike: isLike, user: currentUser });
        }
        else {
            if (likes[0].isLike == isLike) {
                innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
            }
            else {
                innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    .concat({ isLike: isLike, user: currentUser });
            }
        }
        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
                if (likes.length !== 0) {
                    innerPost.reactions.concat(likes);
                }
                return of(innerPost);
            })
        );
    }
}
